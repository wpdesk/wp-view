## [2.1.0] - 2024-06-12
### Added
- Introduced `WooTemplateResolver` for template paths and overrides in WooCommerce spirit.

## [2.0.0] - 2021-10-01
### Added
- Add output_render method to Renderer interface

## [1.1.0] - 2019-09-23
### Added
- PluginViewBuilder to facilitate building and rendering views for plugins
